**This build of st of part of SÅMDE, it's a arch linux post installation script that recreates my desktop setup. I highly recommend checking it out : [SÅMDE](https://gitlab.com/SamDenton/samde)**

# SÅM's build of st

St is the [simple terminal](https://st.suckless.org/) made by suckless. I've applied a few patches to implement the features that I want.

## Patches

- alpha
- boxdraw
- dynamic-cursor-color
- font2
- glyph-wide-support-boxdraw
- scrollback
- scrollback-mouse
- xresources

## Installation
```
git clone --depth 1 https://gitlab.com/SamDenton/st.git
cd st
sudo make clean install
```
